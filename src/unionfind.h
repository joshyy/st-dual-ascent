#ifndef UNIONFIND_H_
#define UNIONFIND_H_

#include <cassert>
#include <unordered_map>
#include <vector>

class unionfind {
private:
  std::unordered_map<int, int> id;
  int n;

public:
  unionfind(int N);
  template <typename ForwardIterator>
  unionfind(ForwardIterator begin, ForwardIterator end) : id(), n(0) {
    for (ForwardIterator it = begin; it != end; ++it) {
      assert(id.count(*it) == 0);
      id[*it] = *it;
      n++;
    }
  }

  int find(int p);
  void unify(int x, int y);
  int nr_sets() const;
};

#endif // UNIONFIND_H_
