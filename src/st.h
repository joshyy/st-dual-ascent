#ifndef ST_H_
#define ST_H_

#include <cassert>
#include <istream>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "graph.h"
#include "hash.h"

class st {
private:
  // Number of edges
  int m;

  // A number, that is larger than the identifiers of all vertices in the st
  int next_free_vertex;

  // Set of the vertices in the st
  std::unordered_set<int> vertices;

  // Set terminal vertices in the st
  std::unordered_set<int> terminals;

  // Sets of neighbors for each vertex
  std::unordered_map<int, std::unordered_set<int>> out;
  std::unordered_map<int, std::unordered_set<int>> in;

  // Set of costs associated with edges
  std::unordered_map<std::pair<int, int>, int> costs;

  // Returns true, if all of the stored data is consistent and false otherwise.
  bool is_consistent() const;

public:
  // ##########################################################################
  // Constructors                                                           ###
  // ##########################################################################

  // Initializes the st without any vertices and edges.
  st();

  // Initializes the st from a SteinLib file parsed from the instream
  st(std::istream &stm);

  // Initializes the st with a set of vertices but no edges
  st(std::unordered_set<int> vertices);

  // Initializes the st with an undirected graph (each undirected edge becomes
  // two directed edges) and a set of terminals.
  st(const graph<> &g, const std::unordered_set<int> terminals);

  // Initializes the st with a set of vertices and terminal nodes but no edges
  st(std::unordered_set<int> vertices, std::unordered_set<int> terminals);

  // ##########################################################################
  // ST Manipulation                                                        ###
  // ##########################################################################

  // Adds an edge between the nodes 'v' and 'w'. Result is undefined, if one of
  // the vertices does not exist or the edge is already present.
  void add_edge(int v, int w);

  // Adds the vertex 'v' to the st. Result is undefined, if the vertex
  // already exists.
  void add_vertex(int v);

  // Adds the vertex 'v' as a terminal node to the st. Result is undefined, if
  // the vertex does not exist.
  void add_terminal(int v);

  // Adds the cost 'c' to edge from 'v' to 'w'. Result is undefined if edge does
  // not exist. Overwrites existing cost. 'v' and 'w' are commutative.
  void add_cost(int v, int w, int c);

  // Removes the edge between 'v' and 'w'. Result is undefined, if one of the
  // vertices does not exist or the edge is not present.
  void rem_edge(int v, int w);

  // Removes the vertex 'v' and all edges 'v' is adjacent to. Result is
  // undefined, if the vertex does not exist.
  void rem_vertex(int v);

  // Removes the vertex 'v' from the list of terminal nodes. Result is
  // undefined if the vertex was not in the list.
  void rem_terminal(int v);

  // Removes the cost from edge 'v' and 'w'. Result is undefined if either
  // edge or cost does not exist.
  void rem_cost(int v, int w);

  // ##########################################################################
  // Obtaining Information                                                  ###
  // ##########################################################################

  // Returns set of vertices
  std::unordered_set<int> get_vertices() const;

  // Returns set of terminal nodes
  std::unordered_set<int> get_terminals() const;

  // Returns set of cost mapping
  std::unordered_map<std::pair<int, int>, int> get_costs() const;

  // Returns the number of vertices in the st.
  int nr_vertices() const;

  // Returns the number of edges in the st.
  int nr_edges() const;

  // Returns true, if 'v' is a vertex of the st and false otherwise.
  bool is_vertex(int v) const;

  // Returns an integer, that can be added as vertex (i.e.
  // is_vertex(free_vertex()) always evaluates to false).
  int free_vertex() const;

  // Returns the cost of any edge between v and w, -1 if no edge is present.
  int cost(int v, int w);

  // Returns the out-degree of the vertex 'v'. Result is undefined, if the
  // vertex does not exist.
  int deg_out(int v) const;

  // Returns the in-degree of the vertex 'v'. Result is undefined, if the vertex
  // does not exist.
  int deg_in(int v) const;

  // Returns true, if 'v' and 'w' are adjacent (i.e. there is an edge in
  // between them), and false otherwise. Result is undefined, if one of the
  // vertices does not exist.
  bool adj(int v, int w) const;

  // Return iterators to iterate through all vertices of the st. Iterators
  // are valid until a change to the st is made.
  std::unordered_set<int>::const_iterator begin() const;
  std::unordered_set<int>::const_iterator end() const;

  // Returns the set of neighbors for the vertex 'v'. A given reference is
  // valid until the digraph is changed.
  const std::unordered_set<int> &neighbors_out(int v) const;
  const std::unordered_set<int> &neighbors_in(int v) const;

  // Returns a set with all vertices, that are reachable from 'v' (including
  // 'v').
  std::unordered_set<int> reachable(int v) const;

  // Returns all outgoing edges from cutset 'cut_set'
  std::unordered_set<std::pair<int, int>>
  out_cuts(std::unordered_set<int> cut_set);
};

#endif // ST_H_
