#ifndef W_H_
#define W_H_

#include "st.h"
#include <unordered_set>

class W {
public:
  int vertex;
  int reach;
  std::unordered_set<int> reachable;
  st *G;

  W();
  W(int v, st *G);

  void clear();
  int update();

  bool operator<(const W &b) const;
  bool operator<=(const W &b) const;
  bool operator>(const W &b) const;
  bool operator>=(const W &b) const;
  bool operator==(const W &b) const;
  bool operator!=(const W &b) const;

  friend std::ostream &operator<<(std::ostream &str, const W &q);
};

#endif // W_H_