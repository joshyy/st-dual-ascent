// ##########################################################################
// Includes                                                               ###
// ##########################################################################

#include "W.h"
#include "graph.h"
#include "hash.h"
#include "st.h"
#include <algorithm>
#include <bits/stdc++.h>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <utility>
#include <vector>

// ##########################################################################
// Typedefs                                                               ###
// ##########################################################################

typedef enum {
  OPTIM_NONE = 0,
  OPTIM_STORED_W_LAZY_FORWARD = 1,
  OPTIM_GRAPH_REDUCTIONS = 2,
  OPTIM_ALL = 3,
} optim_t;

// ##########################################################################
// Prototypes                                                             ###
// ##########################################################################

void read_steinlib(std::istream &istm,
                   std::list<std::pair<std::pair<int, int>, int>> &edges,
                   std::unordered_set<int> &terminals);

// All reduction rules, until not applicable any more
int red(graph<> &g, std::unordered_set<int> &term);

// Vertices of degree 0 or 1
int red1(graph<> &g, std::unordered_set<int> &term);
// Vertices of degree 2
int red2(graph<> &g, std::unordered_set<int> &term);
// Removing an edge, if the distance without the edge is the same or lower.
int red3(graph<> &g, std::unordered_set<int> &term);

int dual_ascent(st G);
int dual_ascent_lazy(st G);
int next_terminal(st G);
void lazy_update(std::vector<W> &W_list);

int primal_heuristic(const graph<> &g, const std::unordered_set<int> &term);

void get_opt(int argc, char **argv, optim_t &o);

// ##########################################################################
// Main                                                                   ###
// ##########################################################################

int main(int argc, char **argv) {
  std::list<std::pair<std::pair<int, int>, int>> edges;
  std::unordered_set<int> terminals;
  read_steinlib(std::cin, edges, terminals);
  graph<> g(edges);
  optim_t opt;
  get_opt(argc, argv, opt);
  int lower_bound = -1;
  

  // Begin time measurement
  clock_t begin = clock();

  // Perform graph reductions
  int r = 0;
  if(opt == OPTIM_ALL || opt == OPTIM_GRAPH_REDUCTIONS){
    std::cout << "Graph size before preprocessing: V " << g.nr_vertices() << " / E "
              << g.nr_edges() << std::endl;
    r = red(g, terminals);
    std::cout << "Graph size after preprocessing: V " << g.nr_vertices() << " / E "
              << g.nr_edges() << std::endl;
  }

  // Calculate upper bound with primal heuristic
  int upper_bound = primal_heuristic(g, terminals) + r;
  std::cout << "Upper Bound: " << upper_bound << std::endl;
  st s(g, terminals);

  // Calculate lower bound with Dual Ascent
  if(opt == OPTIM_STORED_W_LAZY_FORWARD || opt == OPTIM_ALL)
    lower_bound = dual_ascent_lazy(s) + r;
  else
    lower_bound = dual_ascent(s);
  std::cout << "Lower Bound: " << lower_bound << std::endl;

  // End time measurement
  clock_t end = clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  std::cout << "Elapsed seconds: " << elapsed_secs << std::endl;

  return EXIT_SUCCESS;
}

// ##########################################################################
// I/O                                                                    ###
// ##########################################################################

void read_steinlib(std::istream &istm,
                   std::list<std::pair<std::pair<int, int>, int>> &edges,
                   std::unordered_set<int> &terminals) {
  int v1, v2, c;
  int n;
  std::string line, prefix;
  std::stringstream ss;

  while (istm.peek() != EOF) {
    getline(istm, line);
    if (line[0] == '#' || line == "")
      continue;
    if (!istm)
      break;
    ss = std::stringstream(line);
    prefix = "";
    ss >> prefix;
    if (prefix == "Nodes") {
      ss >> n;
    } else if (prefix == "E") {
      ss >> v1 >> v2 >> c;
      v1--;
      v2--;
      edges.push_back(std::make_pair(std::make_pair(v1, v2), c));
    } else if (prefix == "T") {
      ss >> v1;
      v1--;
      terminals.insert(v1);
    }
  }
}

// ##########################################################################
// Reduction rules                                                        ###
// ##########################################################################

int red(graph<> &g, std::unordered_set<int> &term) {
  int r = 0;
  int n, m;
  do {
    n = g.nr_vertices();
    m = g.nr_edges();
    r += red1(g, term);
    r += red2(g, term);
    r += red3(g, term);
  } while (n != g.nr_vertices() || m != g.nr_edges());
  return r;
}

int red1(graph<> &g, std::unordered_set<int> &term) {
  if (term.size() <= 1)
    return 0;

  std::unordered_set<int> vs(g.begin(), g.end());
  int r = 0;
  for (int v : vs) {
    if (g.deg(v) == 0) {
      assert(term.count(v) == 0);
      g.rem_vertex(v);
    } else if (g.deg(v) == 1 && term.count(v) == 0) {
      g.rem_vertex(v);
    } else if (g.deg(v) == 1 && term.count(v) == 1) {
      int n = *g.neighbors(v).begin();
      r += g.cost(v, n);
      term.erase(v);
      term.insert(n);
      g.rem_vertex(v);
    }
  }
  return r;
}

int red2(graph<> &g, std::unordered_set<int> &term) {
  std::unordered_set<int> vs(g.begin(), g.end());
  for (int v : vs)
    if (g.deg(v) == 2 && term.count(v) == 0) {
      auto it = g.neighbors(v).begin();
      int n1 = *it++;
      int n2 = *it;
      int c = g.cost(v, n1) + g.cost(v, n2);
      if (!g.adj(n1, n2))
        g.add_edge(n1, n2, c);
      else if (g.cost(n1, n2) > c)
        g.change_cost(n1, n2, c);
      g.rem_vertex(v);
    }
  return 0;
}

int red3(graph<> &g, std::unordered_set<int> &) {
  std::unordered_set<std::pair<int, int>> es;
  for (int v : g)
    for (int n : g.neighbors(v))
      if (v < n)
        es.insert(std::make_pair(v, n));
  for (std::pair<int, int> e : es) {
    int v = e.first, w = e.second;
    int c = g.cost(v, w);
    g.rem_edge(v, w);
    std::unordered_map<int, int> d = g.dist(v, c);
    if (d.count(w) == 0)
      g.add_edge(v, w, c);
  }
  return 0;
}

// ##########################################################################
// Functions - Dual ascend                                                ###
// ##########################################################################

int dual_ascent_lazy(st G) {
  st G_A = st(G.get_vertices(), G.get_terminals());
  std::unordered_map<std::pair<int, int>, int> c_tilde = G.get_costs();
  std::unordered_set<std::pair<int, int>> out_cuts;
  std::vector<W> W_sorted;
  W W_current;
  int delta;
  int LB = 0;
  int r = *(G_A.get_terminals().begin());
  G_A.rem_terminal(r);
  for (int t : G_A.get_terminals())
    W_sorted.push_back(W(t, &G_A));

  while (W_sorted.size() > 0) {
    lazy_update(W_sorted);
    W_current = W_sorted.back();
    W_current.update();
    if (W_current.reachable.count(r) == 1) {
      W_sorted.pop_back();
      continue;
    }

    delta = INT_MAX;
    out_cuts = G.out_cuts(W_current.reachable);
    for (std::pair<int, int> a : out_cuts) {
      delta = std::min(delta, c_tilde[a]);
    }
    for (std::pair<int, int> a : out_cuts) {
      c_tilde[a] -= delta;
      if (c_tilde[a] == 0)
        G_A.add_edge(a.first, a.second);
    }
    LB += delta;
  }
  return LB;
}

int dual_ascent(st G) {
  st G_A = st(G.get_vertices(), G.get_terminals());
  std::unordered_map<std::pair<int, int>, int> c_tilde = G.get_costs();
  std::unordered_set<std::pair<int, int>> out_cuts;
  std::unordered_set<int> W;
  int delta;
  int LB = 0;
  int r = *(G_A.get_terminals().begin());
  G_A.rem_terminal(r);
  int t;

  while (G_A.get_terminals().size() > 0) {
    t = next_terminal(G_A);
    W = G_A.reachable(t);
    if (W.count(r) == 1) {
      G_A.rem_terminal(t);
      continue;
    }
    delta = INT_MAX;
    out_cuts = G.out_cuts(W);
    for (std::pair<int, int> a : out_cuts) {
      delta = std::min(delta, c_tilde[a]);
    }
    for (std::pair<int, int> a : out_cuts) {
      c_tilde[a] -= delta;
      if (c_tilde[a] == 0)
        G_A.add_edge(a.first, a.second);
    }
    LB += delta;
  }
  return LB;
}

void lazy_update(std::vector<W> &W_list) {
  int cur_best = INT_MAX;
  int c = 0;
  for (auto w = W_list.end(); w-- != W_list.begin();) {
    c++;
    if (w->reach >= cur_best)
      break;
    if (w->update() < cur_best) {
      cur_best = w->reach;
      if (cur_best == 1)
        break;
    }
  }
  std::partial_sort(W_list.rbegin(), W_list.rbegin() + (W_list.size() - c),
                    W_list.rend());
}

int next_terminal(st G) {
  std::pair<int, int> min_reach = std::make_pair(INT_MAX, INT_MAX);
  int reach;
  for (int t : G.get_terminals()) {
    reach = G.reachable(t).size();
    if (reach < min_reach.second)
      min_reach = std::make_pair(t, reach);
  }
  return min_reach.first;
}

// ##########################################################################
// Functions - Heuristic                                                  ###
// ##########################################################################

void clone_vertex(graph<> &g_tar, const graph<> &g_src, int v) {
  assert(g_src.is_vertex(v));
  assert(!g_tar.is_vertex(v));
  g_tar.add_vertex(v);
  for (int n : g_src)
    if (g_tar.is_vertex(n)) {
      if (g_tar.adj(v, n))
        g_tar.change_cost(v, n, g_src.cost(v, n));
      else
        g_tar.add_edge(v, n, g_src.cost(v, n));
    }
}

int primal_heuristic(const graph<> &g, const std::unordered_set<int> &term) {
  graph<> d(g.g_dist());
  graph<> steiner(d.subgraph(term.begin(), term.end()));
  graph<> mst;
  int mst_size = steiner.mst(mst);

  std::vector<std::pair<int, int>> diffs;
  for (int v : g)
    if (!steiner.is_vertex(v)) {
      clone_vertex(steiner, d, v);
      int diff = steiner.mst(mst) - mst_size;
      if (diff < 0)
        diffs.push_back(std::make_pair(diff, v));
      steiner.rem_vertex(v);
    }

  std::sort(diffs.begin(), diffs.end());
  for (std::pair<int, int> p : diffs) {
    int v = p.second;
    clone_vertex(steiner, d, v);
    int diff = steiner.mst(mst) - mst_size;
    if (diff < 0)
      mst_size += diff;
    else
      steiner.rem_vertex(v);
  }

  return mst_size;
}

// ##########################################################################
// Functions - Utility                                                    ###
// ##########################################################################

void get_opt(int argc, char **argv, optim_t &o){
    // Parse parameters
  if (argc != 2) {
    std::cout << "Usage: " << argv[0]
              << " <optimization>"
              << std::endl;
    std::cout << "optimization:" << std::endl
              << "  0 = NO_OPTIMIZATION" << std::endl
              << "  1 = STORED_W_LAZY_FORWARD" << std::endl
              << "  2 = GRAPH_REDUCTIONS" << std::endl
              << "  3 = ALL_OPTIMIZATIONS" << std::endl;
    exit(1);
  }
  o = (optim_t)std::stoi(argv[1]);
}