#include "stud.h"

#include "unionfind.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <list>
#include <sstream>

// ############################################################################
// Reading the adjacency matrix from an instream                            ###
// ############################################################################

std::string next_line(std::istream &stm) {
  std::string line{""};
  do
    getline(stm, line);
  while ((line[0] == 'c' || line[0] == 'C' || line == "") && stm);
  return line;
}

// ############################################################################
// Checking graph consistency                                               ###
// ############################################################################

bool stud::is_consistent() const {
  // 'vertices' is the set of vertices.
  for (int v : vertices)
    if (adj_sets.count(v) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    if (vertices.count(p.first) == 0)
      return false;

  // All neighbors are vertices.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;

  // Each edge is undirected.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (adj_sets.at(v).count(p.first) == 0)
        return false;

  // 'm' is the number of edges.
  int _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    _m += p.second.size() + p.second.count(p.first);
  if (_m != 2 * m)
    return false;

  // 'next_free_vertex' is larger than the identifiers of all vertices
  for (int v : vertices)
    if (next_free_vertex <= v)
      return false;

  // For every edge exists a cost
  if (costs.size() / 2 != (size_t)m)
    return false;

  // Number of terminal nodes is smaller than number of nodes
  if (terminals.size() > vertices.size())
    return false;

  return true;
}

// ############################################################################
// Constructors                                                             ###
// ############################################################################

stud::stud()
    : m(0), next_free_vertex(0), vertices(), terminals(), adj_sets(), costs() {}

stud::stud(std::istream &stm) : stud() {
  int v1, v2, c;
  int n;
  std::string line, prefix;
  std::stringstream ss;

  while (stm.peek() != EOF) {
    line = next_line(stm);
    ss = std::stringstream(line);
    ss >> prefix;
    if (prefix == "Nodes") {
      ss >> n;
      for (int i = 0; i < n; i++)
        add_vertex(i);
    } else if (prefix == "E") {
      ss >> v1 >> v2 >> c;
      v1 = v1 - 1;
      v2 = v2 - 1;
      add_edge(v1, v2);
      add_cost(v1, v2, c);
    } else if (prefix == "T") {
      ss >> v1;
      add_terminal(v1);
    }
  }
  assert(is_consistent());
}

// ############################################################################
// ST Manipulation                                                       ###
// ############################################################################

void stud::add_edge(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 0);
  assert(adj_sets[w].count(v) == 0);
  adj_sets[v].insert(w);
  adj_sets[w].insert(v);
  m++;
}

void stud::add_vertex(int v) {
  assert(vertices.count(v) == 0);
  assert(adj_sets.count(v) == 0);
  vertices.insert(v);
  adj_sets[v];
  assert(v < std::numeric_limits<int>::max());
  if (next_free_vertex <= v)
    next_free_vertex = v + 1;
}

void stud::add_terminal(int v) {
  assert(adj_sets.count(v) == 1);
  assert(terminals.count(v) == 0);
  terminals.insert(v);
}

void stud::add_cost(int v, int w, int c) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 1);
  assert(adj_sets[w].count(v) == 1);
  assert(v < MIN(MAGIC_PRIME_1, MAGIC_PRIME_2));
  assert(w < MIN(MAGIC_PRIME_1, MAGIC_PRIME_2));
  costs.insert(std::make_pair(std::make_pair(v, w), c));
  costs.insert(std::make_pair(std::make_pair(w, v), c));
}

void stud::rem_edge(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 1);
  assert(adj_sets[w].count(v) == 1);
  rem_cost(v, w);
  adj_sets[v].erase(w);
  adj_sets[w].erase(v);
  m--;
}

void stud::rem_vertex(int v) {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  m -= deg(v);
  for (int n : neighbors(v))
    if (n != v) {
      rem_cost(n, v);
      adj_sets[n].erase(v);
    }
  vertices.erase(v);
  adj_sets.erase(v);
}

void stud::rem_terminal(int v) {
  assert(terminals.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  terminals.erase(v);
}

void stud::rem_cost(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  costs.erase(std::make_pair(v, w));
  costs.erase(std::make_pair(w, v));
}

// ############################################################################
// Obtaining Information                                                    ###
// ############################################################################

int stud::nr_vertices() const { return vertices.size(); }

int stud::nr_edges() const { return m; }

bool stud::is_vertex(int v) const { return vertices.count(v) == 1; }

int stud::free_vertex() const { return next_free_vertex; }

int stud::deg(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v).size();
}

bool stud::adj(int v, int w) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(adj_sets.count(w) == 1);
  return adj_sets.at(v).count(w) == 1;
}

std::unordered_set<int>::const_iterator stud::begin() const {
  return vertices.begin();
}
std::unordered_set<int>::const_iterator stud::end() const {
  return vertices.end();
}

const std::unordered_set<int> &stud::neighbors(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v);
}

std::vector<std::vector<int>> stud::conn_comp() const {
  std::vector<std::vector<int>> cc;
  unionfind u(begin(), end());
  for (int v : *this)
    for (int n : neighbors(v))
      u.unify(v, n);
  std::unordered_map<int, int> comp;
  for (int v : *this) {
    int root = u.find(v);
    if (comp.count(root) == 0) {
      cc.resize(cc.size() + 1);
      comp[root] = cc.size() - 1;
    }
    cc.at(comp[root]).push_back(v);
  }
  return cc;
}