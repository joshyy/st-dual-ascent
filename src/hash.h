#ifndef HASH_H_
#define HASH_H_

#include <utility>

#define MAGIC_PRIME_1 (100049)
#define MAGIC_PRIME_2 (100189)

namespace std {
template <> struct hash<std::pair<int, int>> {
  inline size_t operator()(const std::pair<int, int> &v) const {
    std::hash<int> int_hasher;
    return int_hasher(v.first) * MAGIC_PRIME_1 +
           int_hasher(v.second) * MAGIC_PRIME_2;
  }
};
} // namespace std

#endif // HASH_H_
