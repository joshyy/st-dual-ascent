#include "st.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <queue>
#include <sstream>

// ############################################################################
// Reading the adjacency matrix from an instream                            ###
// ############################################################################

std::string next_line(std::istream &stm) {
  std::string line{""};
  do
    getline(stm, line);
  while ((line[0] == 'c' || line[0] == 'C' || line[0] == '#' || line == "") &&
         stm);
  return line;
}

// ############################################################################
// Checking st consistency                                                  ###
// ############################################################################

bool st::is_consistent() const {
  // 'vertices' is the set of vertices.
  for (int v : vertices)
    if (out.count(v) == 0)
      return false;
  for (int v : vertices)
    if (in.count(v) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    if (vertices.count(p.first) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    if (vertices.count(p.first) == 0)
      return false;

  // All neighbors are vertices.
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;

  // 'm' is the number of edges.
  int _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : out)
    _m += p.second.size();
  if (_m != m)
    return false;
  _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : in)
    _m += p.second.size();
  if (_m != m)
    return false;

  // 'next_free_vertex' is larger than the identifiers of all vertices
  for (int v : vertices)
    if (next_free_vertex <= v)
      return false;

  // For every edge exists a cost
  if (costs.size() != (size_t)m)
    return false;

  // Number of terminal nodes is smaller than number of nodes
  if (terminals.size() > vertices.size())
    return false;

  /*
  std::cout << "Edges: " << m << std::endl;
  std::cout << "Nodes: " << vertices.size() << std::endl;
  std::cout << "Costs: " << costs.size() << std::endl;
  std::cout << "Terminals: " << terminals.size() << std::endl;
  */

  return true;
}

// ############################################################################
// Constructors                                                             ###
// ############################################################################

st::st()
    : m(0), next_free_vertex(0), vertices(), terminals(), out(), in(), costs() {
}

st::st(std::unordered_set<int> vertices) : st() {
  for (int v : vertices)
    add_vertex(v);
  assert(is_consistent());
}

st::st(std::unordered_set<int> vertices, std::unordered_set<int> terminals)
    : st(vertices) {
  for (int t : terminals)
    add_terminal(t);
  assert(is_consistent());
}

st::st(std::istream &stm) : st() {
  int v1, v2, c;
  int n;
  std::string line, prefix;
  std::stringstream ss;

  while (stm.peek() != EOF) {
    line = next_line(stm);
    ss = std::stringstream(line);
    prefix = "";
    ss >> prefix;
    if (prefix == "Nodes") {
      ss >> n;
      for (int i = 0; i < n; i++)
        add_vertex(i);
    } else if (prefix == "E") {
      ss >> v1 >> v2 >> c;
      v1 = v1 - 1;
      v2 = v2 - 1;
      add_edge(v1, v2);
      add_edge(v2, v1);
      add_cost(v1, v2, c);
      add_cost(v2, v1, c);
    } else if (prefix == "T") {
      ss >> v1;
      v1--;
      add_terminal(v1);
    }
  }
  assert(is_consistent());
}

st::st(const graph<> &g, const std::unordered_set<int> terminals)
    : m(0), next_free_vertex(0), vertices(), terminals(terminals), out(), in(),
      costs() {
  for (int v : g)
    add_vertex(v);
  for (int v : g)
    for (int n : g.neighbors(v)) {
      add_edge(v, n);
      add_cost(v, n, g.cost(v, n));
    }
}

// ############################################################################
// Graph Manipulation                                                       ###
// ############################################################################

void st::add_edge(int v, int w) {
  assert(out.count(v) == 1);
  assert(in.count(w) == 1);
  assert(out[v].count(w) == 0);
  assert(in[w].count(v) == 0);
  out[v].insert(w);
  in[w].insert(v);
  m++;
}

void st::add_terminal(int v) {
  assert(vertices.count(v) == 1);
  assert(terminals.count(v) == 0);
  terminals.insert(v);
}

void st::add_cost(int v, int w, int c) {
  assert(vertices.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(out[v].count(w) == 1);
  assert(in[w].count(v) == 1);
  assert(v < std::min(MAGIC_PRIME_1, MAGIC_PRIME_2));
  assert(w < std::min(MAGIC_PRIME_1, MAGIC_PRIME_2));
  costs.insert(std::make_pair(std::make_pair(v, w), c));
}

void st::add_vertex(int v) {
  assert(vertices.count(v) == 0);
  assert(out.count(v) == 0);
  assert(in.count(v) == 0);
  vertices.insert(v);
  out[v];
  in[v];
  assert(v < std::numeric_limits<int>::max());
  if (next_free_vertex <= v)
    next_free_vertex = v + 1;
}

void st::rem_edge(int v, int w) {
  assert(out.count(v) == 1);
  assert(in.count(w) == 1);
  assert(out[v].count(w) == 1);
  assert(in[w].count(v) == 1);
  rem_cost(v, w);
  out[v].erase(w);
  in[w].erase(v);
  m--;
}

void st::rem_vertex(int v) {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  assert(in.count(v) == 1);
  m -= deg_out(v);
  m -= deg_in(v);
  for (int n : neighbors_out(v))
    if (n != v) {
      in[n].erase(v);
      rem_cost(v, n);
    }
  for (int n : neighbors_in(v))
    if (n != v) {
      out[n].erase(v);
      rem_cost(n, v);
    }
  vertices.erase(v);
  out.erase(v);
  in.erase(v);
}

void st::rem_terminal(int v) {
  assert(vertices.count(v) == 1);
  assert(terminals.count(v) == 1);
  terminals.erase(v);
}

void st::rem_cost(int v, int w) {
  assert(vertices.count(v) == 1);
  assert(vertices.count(w) == 1);
  costs.erase(std::make_pair(v, w));
}

// ############################################################################
// Obtaining Information                                                    ###
// ############################################################################

std::unordered_set<int> st::get_vertices() const { return vertices; }

std::unordered_set<int> st::get_terminals() const { return terminals; }

std::unordered_map<std::pair<int, int>, int> st::get_costs() const {
  return costs;
}

int st::nr_vertices() const { return vertices.size(); }

int st::nr_edges() const { return m; }

bool st::is_vertex(int v) const { return vertices.count(v) == 1; }

int st::free_vertex() const { return next_free_vertex; }

int st::deg_out(int v) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  return out.at(v).size();
}

int st::deg_in(int v) const {
  assert(vertices.count(v) == 1);
  assert(in.count(v) == 1);
  return in.at(v).size();
}

bool st::adj(int v, int w) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(out.count(w) == 1);
  return out.at(v).count(w) == 1;
}

std::unordered_set<int>::const_iterator st::begin() const {
  return vertices.begin();
}
std::unordered_set<int>::const_iterator st::end() const {
  return vertices.end();
}

const std::unordered_set<int> &st::neighbors_out(int v) const {
  assert(vertices.count(v) == 1);
  assert(out.count(v) == 1);
  return out.at(v);
}

const std::unordered_set<int> &st::neighbors_in(int v) const {
  assert(vertices.count(v) == 1);
  assert(in.count(v) == 1);
  return in.at(v);
}

std::unordered_set<int> st::reachable(int v) const {
  assert(is_vertex(v));
  std::unordered_set<int> r;
  r.insert(v);
  std::queue<int> next;
  next.push(v);
  while (!next.empty()) {
    int w = next.front();
    next.pop();
    for (int n : neighbors_out(w))
      if (r.count(n) == 0) {
        r.insert(n);
        next.push(n);
      }
  }
  return r;
}

std::unordered_set<std::pair<int, int>>
st::out_cuts(std::unordered_set<int> cut_set) {
  std::unordered_set<std::pair<int, int>> arcs;
  for (int v : cut_set)
    for (int w : out[v])
      if (cut_set.count(w) == 0)
        arcs.insert(std::make_pair(v, w));
  return arcs;
}
