#ifndef GRAPH_H_
#define GRAPH_H_

#include <cassert>
#include <istream>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "hash.h"

template <typename C = int> class graph {
private:
  // Number of edges
  int m;

  // A number, that is larger than the identifiers of all vertices in the
  // graph
  int next_free_vertex;

  // Set of the vertices in the graph
  std::unordered_set<int> vertices;

  // Sets of neighbors for each vertex
  std::unordered_map<int, std::unordered_set<int>> adj_sets;

  // Set of costs associated with edges
  std::unordered_map<std::pair<int, int>, C> costs;

  // Returns true, if all of the stored data is consistent and false otherwise.
  bool is_consistent() const;

public:
  // ##########################################################################
  // Constructors                                                           ###
  // ##########################################################################

  // Initializes the graph without any vertices and edges.
  graph();

  // Initializes the graph with n vertices with no edges in between. The
  // identifiers of the vertices are given by {0, 1, ..., n-1}.
  graph(int n);

  // Initializes the graph with the edges given by 'edges'.
  graph(const std::list<std::pair<std::pair<int, int>, C>> &edges);

  // ##########################################################################
  // Graph Manipulation                                                     ###
  // ##########################################################################

  // Adds an edge between the nodes 'v' and 'w'. Result is undefined, if one of
  // the vertices does not exist or the edge is already present.
  void add_edge(int v, int w, C cost);

  // Changes the cost of an existing edge and returns the old cost.
  C change_cost(int v, int w, C cost);

  // Adds the vertex 'v' to the graph. Result is undefined, if the vertex
  // already exists.
  void add_vertex(int v);

  // Removes the edge between 'v' and 'w'. Result is undefined, if one of the
  // vertices does not exist or the edge is not present.
  void rem_edge(int v, int w);

  // Removes the vertex 'v' and all edges 'v' is adjacent to. Result is
  // undefined, if the vertex does not exist.
  void rem_vertex(int v);

  // ##########################################################################
  // Obtaining Information                                                  ###
  // ##########################################################################

  // Returns the number of vertices in the graph.
  int nr_vertices() const;

  // Returns the number of edges in the graph.
  int nr_edges() const;

  // Returns true, if 'v' is a vertex of the graph and false otherwise.
  bool is_vertex(int v) const;

  // Returns an integer, that can be added as vertex (i.e.
  // is_vertex(free_vertex()) always evaluates to false).
  int free_vertex() const;

  // Returns the degree of the vertex 'v'. Result is undefined, if the vertex
  // does not exist.
  int deg(int v) const;

  // Returns true, if 'v' and 'w' are adjacent (i.e. there is an edge in
  // between them), and false otherwise. Result is undefined, if one of the
  // vertices does not exist.
  bool adj(int v, int w) const;

  // Returns the cost of the edge between v and w, -1 if no edge is present.
  C cost(int v, int w) const;

  // Return iterators to iterate through all vertices of the graph. Iterators
  // are valid until a change to the graph is made.
  std::unordered_set<int>::const_iterator begin() const;
  std::unordered_set<int>::const_iterator end() const;

  // Returns the set of neighbors for the vertex 'v' (or the vertices given by
  // 'begin' and 'end'). For the first version the set is valid until the graph
  // is changed. 'closed_neighbors' also includes the given vertices.
  const std::unordered_set<int> &neighbors(int v) const;

  // Get a subgraph of the graph.
  template <typename ForwardIterator>
  graph<C> subgraph(ForwardIterator begin, ForwardIterator end) const;

  // ##########################################################################
  // Algorithms                                                             ###
  // ##########################################################################

  // Calculates a minimal spanning tree using the algorithm of Kruskal.
  C mst(graph<C> &_mst) const;

  // Calculates the shortest distances to 'v' using the algorithm of Dijkstra.
  // 'max_d' indicates the maximal distance, to which the calculation runs.
  std::unordered_map<int, C> dist(int v) const;
  std::unordered_map<int, C> dist(int v, C max_d) const;

  // Complete graph with the distances between the vertices. Graph needs to be
  // connected. 'max_d' indicates the maximal cost of each newly created edge.
  // All edges with higher cost are dropped.
  graph<C> g_dist() const;
  graph<C> g_dist(C max_d) const;
};

template <typename C>
template <typename ForwardIterator>
graph<C> graph<C>::subgraph(ForwardIterator begin, ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  graph<C> s;
  for (ForwardIterator it = begin; it != end; ++it)
    s.add_vertex(*it);
  for (ForwardIterator it = begin; it != end; ++it)
    for (int n : neighbors(*it))
      if (*it <= n && s.is_vertex(n))
        s.add_edge(*it, n, cost(*it, n));
  assert(s.is_consistent());
  return s;
}

#endif // GRAPH_H_
