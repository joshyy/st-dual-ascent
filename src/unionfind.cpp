#include "unionfind.h"

unionfind::unionfind(int N) : id(N) {
  assert(N >= 0);
  for (int i = 0; i < N; i++)
    id[i] = i;
}

int unionfind::find(int p) {
  assert(id.count(p) == 1);
  int root = p;
  while (root != id[root])
    root = id[root];
  while (p != root) {
    int _p = id[p];
    id[p] = root;
    p = _p;
  }
  return root;
}

void unionfind::unify(int x, int y) {
  assert(id.count(x) == 1);
  assert(id.count(x) == 1);
  int rx = find(x), ry = find(y);
  if (rx != ry) {
    id[rx] = ry;
    n--;
  }
}

int unionfind::nr_sets() const { return n; }
