#include "graph.h"

#include "unionfind.h"

#include <algorithm>
#include <limits>
#include <list>
#include <queue>
#include <sstream>

// ############################################################################
// Checking graph consistency                                               ###
// ############################################################################

template <typename C> bool graph<C>::is_consistent() const {
  // 'vertices' is the set of vertices.
  for (int v : vertices)
    if (adj_sets.count(v) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    if (vertices.count(p.first) == 0)
      return false;

  // All neighbors are vertices.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;

  // Each edge is undirected.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (adj_sets.at(v).count(p.first) == 0)
        return false;

  // 'm' is the number of edges.
  int _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    _m += p.second.size() + p.second.count(p.first);
  if (_m != 2 * m)
    return false;

  // 'next_free_vertex' is larger than the identifiers of all vertices
  for (int v : vertices)
    if (next_free_vertex <= v)
      return false;

  return true;
}

// ############################################################################
// Constructors                                                             ###
// ############################################################################

template <typename C>
graph<C>::graph()
    : m(0), next_free_vertex(0), vertices(), adj_sets(), costs() {}

template <typename C>
graph<C>::graph(int n)
    : m(0), next_free_vertex(n), vertices(n), adj_sets(n), costs() {
  for (int i = 0; i < n; i++)
    add_vertex(i);
  assert(is_consistent());
}

template <typename C>
graph<C>::graph(const std::list<std::pair<std::pair<int, int>, C>> &edges)
    : m(0), next_free_vertex(0), vertices(), adj_sets(), costs() {
  for (std::pair<std::pair<int, int>, C> e : edges) {
    int v = e.first.first, w = e.first.second;
    if (!is_vertex(v))
      add_vertex(v);
    if (!is_vertex(w))
      add_vertex(w);
    if (!adj(v, w))
      add_edge(v, w, e.second);
  }
  assert(is_consistent());
}

// ############################################################################
// Graph Manipulation                                                       ###
// ############################################################################

template <typename C> void graph<C>::add_edge(int v, int w, C cost) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 0);
  assert(adj_sets[w].count(v) == 0);
  adj_sets[v].insert(w);
  if (v != w)
    adj_sets[w].insert(v);
  costs[std::make_pair(std::min(v, w), std::max(v, w))] = cost;
  m++;
}

template <typename C> C graph<C>::change_cost(int v, int w, C cost) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 1);
  assert(adj_sets[w].count(v) == 1);
  C c = costs[std::make_pair(std::min(v, w), std::max(v, w))];
  costs[std::make_pair(std::min(v, w), std::max(v, w))] = cost;
  m++;
  return c;
}

template <typename C> void graph<C>::add_vertex(int v) {
  assert(vertices.count(v) == 0);
  assert(adj_sets.count(v) == 0);
  vertices.insert(v);
  adj_sets[v];
  assert(v < std::numeric_limits<int>::max());
  if (next_free_vertex <= v)
    next_free_vertex = v + 1;
}

template <typename C> void graph<C>::rem_edge(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 1);
  assert(adj_sets[w].count(v) == 1);
  adj_sets[v].erase(w);
  adj_sets[w].erase(v);
  m--;
}

template <typename C> void graph<C>::rem_vertex(int v) {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  m -= deg(v);
  for (int n : neighbors(v))
    if (n != v)
      adj_sets[n].erase(v);
  vertices.erase(v);
  adj_sets.erase(v);
}

// ############################################################################
// Obtaining Information                                                    ###
// ############################################################################

template <typename C> int graph<C>::nr_vertices() const {
  return vertices.size();
}

template <typename C> int graph<C>::nr_edges() const { return m; }

template <typename C> bool graph<C>::is_vertex(int v) const {
  return vertices.count(v) == 1;
}

template <typename C> int graph<C>::free_vertex() const {
  return next_free_vertex;
}

template <typename C> int graph<C>::deg(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v).size();
}

template <typename C> bool graph<C>::adj(int v, int w) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(adj_sets.count(w) == 1);
  return adj_sets.at(v).count(w) == 1;
}

template <typename C> C graph<C>::cost(int v, int w) const {
  if (adj(v, w))
    return costs.at(std::make_pair(std::min(v, w), std::max(v, w)));
  else
    return -1;
}

template <typename C>
std::unordered_set<int>::const_iterator graph<C>::begin() const {
  return vertices.begin();
}
template <typename C>
std::unordered_set<int>::const_iterator graph<C>::end() const {
  return vertices.end();
}

template <typename C>
const std::unordered_set<int> &graph<C>::neighbors(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v);
}

// ############################################################################
// Algorithms                                                               ###
// ############################################################################

template <typename C> C graph<C>::mst(graph<C> &_mst) const {
  _mst = graph<C>();
  for (int v : *this)
    _mst.add_vertex(v);

  std::vector<std::pair<C, std::pair<int, int>>> edges;
  for (int v : *this)
    for (int n : neighbors(v))
      if (v <= n)
        edges.push_back(std::make_pair(cost(v, n), std::make_pair(v, n)));
  std::sort(edges.begin(), edges.end());

  unionfind conn(begin(), end());
  C size = (C)0;
  for (std::pair<C, std::pair<int, int>> p : edges) {
    int v = p.second.first, w = p.second.second;
    C c = p.first;
    if (conn.find(v) != conn.find(w)) {
      _mst.add_edge(v, w, c);
      size += c;
      conn.unify(v, w);
    }
  }
  return size;
}

template <typename C> std::unordered_map<int, C> graph<C>::dist(int v) const {
  return dist(v, std::numeric_limits<C>::max());
}

template <typename C>
std::unordered_map<int, C> graph<C>::dist(int v, C max_d) const {
  assert(is_vertex(v));
  typedef std::pair<C, int> T;
  std::priority_queue<T, std::vector<T>, std::greater<T>> next;
  std::unordered_map<int, C> d;

  d[v] = (C)0;
  next.push(std::make_pair((C)0, v));
  while (!next.empty()) {
    C c = next.top().first;
    int v = next.top().second;
    next.pop();
    if (d[v] < c)
      continue;
    assert(d[v] == c);
    for (int n : neighbors(v))
      if ((d.count(n) == 0 || c + cost(v, n) < d[n]) &&
          c + cost(v, n) <= max_d) {
        d[n] = c + cost(v, n);
        next.push(std::make_pair(d[n], n));
      }
  }

  return d;
}

template <typename C> graph<C> graph<C>::g_dist() const {
  return g_dist(std::numeric_limits<C>::max());
}

template <typename C> graph<C> graph<C>::g_dist(C max_d) const {
  graph<C> d;
  for (int v : *this)
    d.add_vertex(v);

  for (int v : *this) {
    std::unordered_map<int, C> v_d(dist(v, max_d));
    for (std::pair<int, C> p : v_d)
      if (v < p.first)
        d.add_edge(v, p.first, p.second);
  }

  return d;
}

template class graph<int>;
template class graph<double>;
