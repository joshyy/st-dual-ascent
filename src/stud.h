#ifndef STUD_H_
#define STUD_H_

#include <cassert>
#include <istream>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#define MAGIC_PRIME_1 100049
#define MAGIC_PRIME_2 100189
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

namespace std {
template <> struct hash<std::pair<int, int>> {
  inline size_t operator()(const std::pair<int, int> &v) const {
    std::hash<int> int_hasher;
    return int_hasher(v.first) * MAGIC_PRIME_1 +
           int_hasher(v.second) * MAGIC_PRIME_2;
  }
};
} // namespace std

class stud {
private:
  // Number of edges
  int m;

  // A number, that is larger than the identifiers of all vertices in the
  // st
  int next_free_vertex;

  // Set of the vertices in the st
  std::unordered_set<int> vertices;

  // Set terminal vertices in the st
  std::unordered_set<int> terminals;

  // Sets of neighbors for each vertex
  std::unordered_map<int, std::unordered_set<int>> adj_sets;

  // Set of costs associated with edges
  std::unordered_map<std::pair<int, int>, int> costs;

  // Returns true, if all of the stored data is consistent and false otherwise.
  bool is_consistent() const;

public:
  // ##########################################################################
  // Constructors                                                           ###
  // ##########################################################################

  // Initializes the stud without any vertices and edges.
  stud();

  // Initializes the stud from a SteinLib file parsed from the instream
  stud(std::istream &stm);

  // ##########################################################################
  // stud Manipulation                                                     ###
  // ##########################################################################

  // Adds an edge between the nodes 'v' and 'w'. Result is undefined, if one of
  // the vertices does not exist or the edge is already present.
  void add_edge(int v, int w);

  // Adds the vertex 'v' to the st. Result is undefined, if the vertex
  // already exists.
  void add_vertex(int v);

  // Adds the vertex 'v' as a terminal node to the st. Result is undefined, if
  // the vertex does not exist.
  void add_terminal(int v);

  // Adds the cost 'c' to edge from 'v' to 'w'. Result is undefined if edge does
  // not exist. Overwrites existing cost. 'v' and 'w' are kommutative.
  void add_cost(int v, int w, int c);

  // Removes the edge between 'v' and 'w'. Result is undefined, if one of the
  // vertices does not exist or the edge is not present.
  void rem_edge(int v, int w);

  // Removes the vertex 'v' and all edges 'v' is adjacent to. Result is
  // undefined, if the vertex does not exist.
  void rem_vertex(int v);

  // Removes the vertex 'v' from the list of terminal nodes. Result is
  // undefined if the vertex was not in the list.
  void rem_terminal(int v);

  // Removes the cost from edge 'v' and 'w'. Result is undefined if either
  // edge or cost does not exist.
  void rem_cost(int v, int w);

  // ##########################################################################
  // Obtaining Information                                                  ###
  // ##########################################################################

  // Returns the number of vertices in the st.
  int nr_vertices() const;

  // Returns the number of edges in the st.
  int nr_edges() const;

  // Returns true, if 'v' is a vertex of the stud and false otherwise.
  bool is_vertex(int v) const;

  // Returns an integer, that can be added as vertex (i.e.
  // is_vertex(free_vertex()) always evaluates to false).
  int free_vertex() const;

  // Returns the degree of the vertex 'v'. Result is undefined, if the vertex
  // does not exist.
  int deg(int v) const;

  // Returns true, if 'v' and 'w' are adjacent (i.e. there is an edge in
  // between them), and false otherwise. Result is undefined, if one of the
  // vertices does not exist.
  bool adj(int v, int w) const;

  // Return iterators to iterate through all vertices of the st. Iterators
  // are valid until a change to the stud is made.
  std::unordered_set<int>::const_iterator begin() const;
  std::unordered_set<int>::const_iterator end() const;

  // Returns the set of neighbors for the vertex 'v' (or the vertices given by
  // 'begin' and 'end'). For the first version the set is valid until the st
  // is changed. 'closed_neighbors' also includes the given vertices.
  const std::unordered_set<int> &neighbors(int v) const;
  template <typename ForwardIterator>
  std::unordered_set<int> neighbors(ForwardIterator begin,
                                    ForwardIterator end) const;
  template <typename ForwardIterator>
  std::unordered_set<int> closed_neighbors(ForwardIterator begin,
                                           ForwardIterator end) const;

  // Returns a vector of all connected components of the st.
  std::vector<std::vector<int>> conn_comp() const;

  // Get a subst of the st.
  template <typename ForwardIterator>
  stud subst(ForwardIterator begin, ForwardIterator end) const;
};

template <typename ForwardIterator>
std::unordered_set<int> stud::neighbors(ForwardIterator begin,
                                        ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  std::unordered_set<int> ns;
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(neighbors(*it).begin(), neighbors(*it).end());
  for (ForwardIterator it = begin; it != end; ++it)
    ns.erase(*it);

  return ns;
}
template <typename ForwardIterator>
std::unordered_set<int> stud::closed_neighbors(ForwardIterator begin,
                                               ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  std::unordered_set<int> ns;
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(neighbors(*it).begin(), neighbors(*it).end());
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(*it);

  return ns;
}

template <typename ForwardIterator>
stud stud::subst(ForwardIterator begin, ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  stud s;
  for (ForwardIterator it = begin; it != end; ++it)
    s.add_vertex(*it);
  for (ForwardIterator it = begin; it != end; ++it)
    for (int n : neighbors(*it))
      if (*it <= n && s.is_vertex(n))
        s.add_edge(*it, n);
  assert(s.is_consistent());
  return s;
}

#endif // STUD_H_
