#include "W.h"
#include <iostream>

W::W() : vertex(0), reach(0), reachable(), G(NULL) {}

W::W(int v, st *G) {
  this->G = G;
  this->vertex = v;
  this->reachable = this->G->reachable(v);
  this->reach = this->reachable.size();
}

void W::clear() {
  vertex = 0;
  reach = 0;
  for (int v : reachable)
    reachable.erase(v);
}

int W::update() {
  reachable = G->reachable(vertex);
  reach = reachable.size();
  return reach;
}

bool W::operator<(const W &b) const { return this->reach < b.reach; }

bool W::operator<=(const W &b) const { return this->reach <= b.reach; }

bool W::operator>(const W &b) const { return this->reach > b.reach; }

bool W::operator>=(const W &b) const { return this->reach >= b.reach; }

bool W::operator==(const W &b) const { return this->reach == b.reach; }

bool W::operator!=(const W &b) const { return this->reach != b.reach; }

std::ostream &operator<<(std::ostream &str, const W &q) {
  str << "Node: " << q.vertex << std::endl;
  str << "Reach: " << q.reach << std::endl;
  return str;
}
